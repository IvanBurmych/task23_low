﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace task23_low.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Guest()
        {
            ViewBag.Message = "Your Guest page.";

            return View();
        }

        public ActionResult Questions()
        {
            ViewBag.Message = "Your Questions page.";

            return View();
        }
        [HttpGet]
        public ActionResult qQuestions(string color)
        {
            string authData = $"color: {color}";
            return Content(authData);
        }
        [HttpPost]
        public ActionResult qQuestions(string[] colors)
        {
            string authData = "colors: ";
            foreach (string color in colors)
            {
                authData += $"{color}, ";
            }
            return Content(authData);
        }
    }
}